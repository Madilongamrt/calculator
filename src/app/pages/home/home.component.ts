import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

/*
r - the monthly interest rate, expressed as a decimal, not a percentage.
Since the quoted yearly percentage rate is not a compounded rate, t
he monthly percentage rate is simply the yearly percentage rate divided by 12;
dividing the monthly percentage rate by 100 gives r, the monthly rate expressed as a decimal.
N - the number of monthly payments, called the loan's term, and
P - the amount borrowed, known as the loan's principal.


For example, for a home loan of $200,000 with a fixed yearly interest rate of 6.5% for 30 years,
the principal is {\displaystyle P=200000}P=200000,
the monthly interest rate is {\displaystyle r=(6.5/12)/100}r=(6.5/12)/100,
the number of monthly payments is {\displaystyle N=30\cdot 12=360}N=30\cdot 12=360,
the fixed monthly payment equals $1,264.14.
This formula is provided using the financial function PMT in a spreadsheet such as Excel.
 In the example, the monthly payment is obtained by entering either of these formulas:


 = -PMT(6.5 / 100 / 12, 30 * 12, 200000)
= ((6.5 / 100 / 12) * 200000) / (1 - ((1 + (6.5 / 100 / 12)) ^ (-30 * 12)))
= 1264.14

*/

  angForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.onClickSubmit(fb);

  }

  onClickSubmit(fb) {
    this.angForm = this.fb.group({
     
    })
  }

  ngOnInit() {

    let borrowUpTo: number;
    let estimated: number;

  }
// method
  //onClickSubmit(formData) {
    //console.log(formData.price, formData.deposit, formData.years, formData.interest);
  //}

}
